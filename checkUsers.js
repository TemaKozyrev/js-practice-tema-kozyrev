function checkUsersValid (goodUsers) {
  return function allUsersValid (submittedUsers) {
    return submittedUsers.every((user) => {
      return goodUsers.some((existUser) => {
        return existUser.id === user.id
      })
    })
  }
}

module.exports = checkUsersValid
