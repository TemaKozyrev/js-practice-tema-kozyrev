function getShortMessages (messages) {
  let result = messages.filter((msg) => {
    return msg.message.length < 50
  })
  return result.map((msg) => {
    return msg.message
  })
}

module.exports = getShortMessages
