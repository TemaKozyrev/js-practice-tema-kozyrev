function doubleAll (numbers) {
  let result = numbers.map((val) => {
    return val * 2
  })
  return result
}

module.exports = doubleAll
